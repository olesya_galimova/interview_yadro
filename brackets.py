def __is_open_bracket(sym):
    return sym in ['(', '{', '[']


def __is_close_bracket(sym):
    return sym in [')', '}', ']']


def is_correct_brackets(string):
    stack = []
    brackets_pairs = {'(': ')', '{': '}', '[': ']'}
    for sym in string:
        if __is_open_bracket(sym):
            stack.append(sym)
        elif __is_close_bracket(sym):
            if sym == brackets_pairs[stack[-1]]:
                stack.pop()
            else:
                break
    return len(stack) == 0
