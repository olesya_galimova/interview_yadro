from brackets import is_correct_brackets

if __name__ == '__main__':
    string = input()
    while string:
        print(is_correct_brackets(string))
        string = input()
